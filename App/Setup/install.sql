SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `therapies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__therapies` ;

CREATE TABLE IF NOT EXISTS `#__therapies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NULL,
  `level` INT(10) NULL,
  `lft` INT(11) NULL,
  `rgt` INT(11) NULL,
  `parent_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `key_title` (`title` ASC),
  INDEX `fk_therapies_parent_idx` (`parent_id` ASC),
  INDEX `key_level` (`level` ASC),
  INDEX `key_lft` (`lft` ASC),
  INDEX `key_rgt` (`rgt` ASC),
  CONSTRAINT `fk_therapies_parent`
    FOREIGN KEY (`parent_id`)
    REFERENCES `#__therapies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__users` ;

CREATE TABLE IF NOT EXISTS `#__users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL,
  `phone` VARCHAR(30) NULL,
  `mobile` VARCHAR(30) NULL,
  `address` VARCHAR(100) NULL,
  `city` VARCHAR(50) NULL,
  `jmbg` VARCHAR(13) NULL,
  `lk` VARCHAR(13) NULL,
  `email` VARCHAR(50) NULL,
  `barcode` VARCHAR(13) NULL,
  `created_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_created_idx` (`created_by` ASC),
  UNIQUE INDEX `uprn_UNIQUE` (`jmbg` ASC),
  CONSTRAINT `fk_users_created`
    FOREIGN KEY (`created_by`)
    REFERENCES `#__users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `user_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__user_groups` ;

CREATE TABLE IF NOT EXISTS `#__user_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `rules` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users_map`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__users_map` ;

CREATE TABLE IF NOT EXISTS `#__users_map` (
  `user_id` INT(11) NOT NULL,
  `user_group_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `user_group_id`),
  INDEX `fk_user_group_idx` (`user_group_id` ASC),
  CONSTRAINT `fk_users_map_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_map_user_group`
    FOREIGN KEY (`user_group_id`)
    REFERENCES `#__user_groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__files` ;

CREATE TABLE IF NOT EXISTS `#__files` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `patient_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_files_patient_idx` (`patient_id` ASC),
  CONSTRAINT `fk_files_patient`
    FOREIGN KEY (`patient_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `examinations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__examinations` ;

CREATE TABLE IF NOT EXISTS `#__examinations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `file_id` INT(11) NOT NULL,
  `stuff_id` INT(11) NULL,
  `comment` LONGTEXT NULL,
  `date` DATETIME NOT NULL,
  `length` VARCHAR(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_examinations_file_idx` (`file_id` ASC),
  INDEX `fk_examinations_stuff_idx` (`stuff_id` ASC),
  INDEX `key_date` (`date` ASC),
  CONSTRAINT `fk_examinations_file`
    FOREIGN KEY (`file_id`)
    REFERENCES `#__files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_examinations_stuff`
    FOREIGN KEY (`stuff_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diagnosis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__diagnosis` ;

CREATE TABLE IF NOT EXISTS `#__diagnosis` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `file_id` INT(11) NOT NULL,
  `stuff_id` INT(11) NULL,
  `comment` LONGTEXT NULL,
  `date` DATETIME NOT NULL,
  `exam_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_diagnosis_file_idx` (`file_id` ASC),
  INDEX `fk_diagnosis_stuff_idx` (`stuff_id` ASC),
  INDEX `fk_diagnosis_exam_idx` (`exam_id` ASC),
  INDEX `key_date` (`date` ASC),
  CONSTRAINT `fk_diagnosis_file`   FOREIGN KEY (`file_id`)
    REFERENCES `#__files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_diagnosis_stuff`
    FOREIGN KEY (`stuff_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_diagnosis_exam`
    FOREIGN KEY (`exam_id`)
    REFERENCES `#__examinations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `treatments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__treatments` ;

CREATE TABLE IF NOT EXISTS `#__treatments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `stuff_id` INT(11) NULL,
  `comment` LONGTEXT NULL,
  `date` DATETIME NULL,
  `therapy_id` INT(11) NOT NULL,
  `diagnose_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_treatments_stuff_idx` (`stuff_id` ASC),
  INDEX `fk_treatments_therapy_idx` (`therapy_id` ASC),
  INDEX `fk_treatments_diagnose_idx` (`diagnose_id` ASC),
  CONSTRAINT `fk_treatments_stuff`
    FOREIGN KEY (`stuff_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_treatments_therapy`
    FOREIGN KEY (`therapy_id`)
    REFERENCES `#__therapies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_treatments_diagnose`
    FOREIGN KEY (`diagnose_id`)
    REFERENCES `#__diagnosis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__payments` ;

CREATE TABLE IF NOT EXISTS `#__payments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `amount` DECIMAL(6,2) NULL DEFAULT 0,
  `status` TINYINT(3) NULL DEFAULT 0,
  `date_issued` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_changed` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payments_user_idx` (`user_id` ASC),
  INDEX `key_status` (`status` ASC),
  CONSTRAINT `fk_payments_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `#__users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `reports`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__reports` ;

CREATE TABLE `#__reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` mediumtext NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reports_user_idx` (`created_by`),
  CONSTRAINT `fk_reports_user`
    FOREIGN KEY (`created_by`)
    REFERENCES `#__users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `calendar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `#__calendar` (
  `id` INT(11) NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `start` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end` DATETIME NOT NULL,
  `comment` MEDIUMTEXT NULL,
  `color` VARCHAR(50) NULL,
  `personnel` INT(11) NOT NULL,
  `patient` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `type` (`type` ASC),
  INDEX `start` (`start` ASC),
  INDEX `end` (`end` ASC),
  INDEX `fk_calendar_personnel_idx` (`personnel` ASC),
  INDEX `fk_calendar_patient_idx` (`patient` ASC),
  CONSTRAINT `fk_calendar_personnel`
  FOREIGN KEY (`personnel`)
  REFERENCES `#__users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendar_patient`
  FOREIGN KEY (`patient`)
  REFERENCES `#__users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
