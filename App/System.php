<?php
/**
 * @copyright  Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

namespace App;

use Joomla\Database\DatabaseDriver;
use Joomla\Date\Date;
use Joomla\DI\Container;
use Joomla\Registry\Registry;
use Joomla\Uri\Uri;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Platform Factory class.
 *
 * @since  1.0
 */
abstract class System
{
	/**
	 * Global application object
	 *
	 * @var    App
	 * @since  1.0
	 */
	private static $application = null;

	/**
	 * Global configuration object
	 *
	 * @var    array
	 * @since  1.0
	 */
	private static $config = null;

	/**
	 * Global session object
	 *
	 * @var    Session
	 * @since  1.0
	 */
	private static $session = null;

	/**
	 * Global database object
	 *
	 * @var    DatabaseDriver
	 * @since  1.0
	 */
	private static $database = null;

	/**
	 * Container object.
	 *
	 * @var    Container
	 * @since  1.0
	 */
	private static $container = null;

	/**
	 * Get a application object.
	 *
	 * @return  false|App object
	 *
	 * @see     App
	 * @since   1.0
	 */
	public static function getApplication()
	{
		if (!self::$application)
		{
			return false;
		}

		return self::$application;
	}

	/**
	 * Set application object.
	 *
	 * @param   App  $app  Application object.
	 *
	 * @return  void
	 */
	public static function setApplication($app)
	{
		self::$application = $app;
		self::$container   = $app->getContainer();
	}

	/**
	 * Getter method for container object.
	 *
	 * @return  false|Container
	 */
	public static function getContainer()
	{
		if (!self::$container)
		{
			return false;
		}

		return self::$container;
	}

	/**
	 * Get a configuration object.
	 *
	 * @return  Registry
	 *
	 * @see     Registry
	 * @since   1.0
	 */
	public static function getConfig()
	{
		if (!isset(self::$config))
		{
			if (!self::getApplication() || !self::getContainer())
			{
				return null;
			}

			self::$config = new Registry;
			self::$config = self::$config->merge(self::$container->get('config'));
		}

		return self::$config;
	}

	/**
	 * Get a session object.
	 *
	 * Returns the global {@link JSession} object, only creating it if it doesn't already exist.
	 *
	 * @return  Session object
	 *
	 * @see     false|Session
	 * @since   1.0
	 */
	public static function getSession()
	{
		if (!self::$session)
		{
			if (!self::getApplication())
			{
				return false;
			}

			self::$session = self::$application->getSession();
		}

		return self::$session;
	}

	/**
	 * Get an user object.
	 *
	 * @param   integer  $id  The user to load - Can be an integer or string - If string, it is converted to ID automatically.
	 *
	 * @return  object User object.
	 *
	 * @since   1.0
	 */
	public static function getUser($id = null)
	{
		// TODO: implement method
		return null;
	}

	/**
	 * Get a database object.
	 *
	 * Returns the global {@link DatabaseDriver} object, only creating it if it doesn't already exist.
	 *
	 * @return  DatabaseDriver
	 *
	 * @see     DatabaseDriver
	 * @since   11.1
	 */
	public static function getDbo()
	{
		if (!self::$database)
		{
			self::$database = self::createDbo();
		}

		return self::$database;
	}

	/**
	 * Reads a XML file.
	 *
	 * @param   string   $data    Full path and file name.
	 * @param   boolean  $isFile  true to load a file or false to load a string.
	 *
	 * @return  mixed    JXMLElement or SimpleXMLElement on success or false on error.
	 *
	 * @see     SimpleXMLElement
	 * @since   1.0
	 */
	public static function getXml($data, $isFile = true)
	{
		$class = 'SimpleXMLElement';

		// Disable libxml errors and allow to fetch error information as needed
		libxml_use_internal_errors(true);

		if ($isFile)
		{
			// Try to load the XML file
			$xml = simplexml_load_file($data, $class);
		}
		else
		{
			// Try to load the XML string
			$xml = simplexml_load_string($data, $class);
		}

		return $xml;
	}

	/**
	 * Return a reference to the {@link Uri} object
	 *
	 * @param   string  $uri  Uri name.
	 *
	 * @return  Uri object
	 *
	 * @see     Uri
	 * @since   1.0
	 */
	public static function getUri($uri = 'SERVER')
	{
		return new Uri($uri);
	}

	/**
	 * Return the {@link Date} object
	 *
	 * @param   mixed  $time      The initial time for the JDate object
	 * @param   mixed  $tzOffset  The timezone offset.
	 *
	 * @return  Date object
	 *
	 * @see     Date
	 * @since   1.0
	 */
	public static function getDate($time = 'now', $tzOffset = null)
	{
		if (is_null($tzOffset))
		{
			$tzOffset = new \DateTimeZone('Europe/Belgrade');
		}

		$date = new Date($time, $tzOffset);

		return $date;
	}

	/**
	 * Shortcut function for application enqueueMessage function.
	 *
	 * @param   string  $msg   Message body.
	 * @param   string  $type  Message type.
	 *
	 * @return  bool  True on success, false otherwise.
	 */
	public static function enqMsg($msg, $type = 'message')
	{
		if (!empty($msg) && self::getApplication())
		{
			self::$application->enqueueMessage($msg, $type);

			return true;
		}

		return false;
	}

	/**
	 * Create an database object.
	 *
	 * @return  DatabaseDriver
	 *
	 * @see     DatabaseDriver
	 * @since   1.0
	 */
	protected static function createDbo()
	{
		$conf     = self::getConfig();
		$host     = $conf->get('database.host');
		$user     = $conf->get('database.user');
		$password = $conf->get('database.password');
		$database = $conf->get('database.name');
		$prefix   = $conf->get('database.prefix');
		$driver   = $conf->get('database.driver');
		$options  = array (
			'driver'   => $driver,
			'host'     => $host,
			'user'     => $user,
			'password' => $password,
			'database' => $database,
			'prefix'   => $prefix
		);

		try
		{
			$db = DatabaseDriver::getInstance($options);
		}
		catch (\RuntimeException $e)
		{
			if (!headers_sent())
			{
				header('HTTP/1.1 500 Internal Server Error');
			}

			die('Database Error: ' . $e->getMessage());
		}

		return $db;
	}
}
