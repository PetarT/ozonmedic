<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Controller;

use App\Model\BaseModel;
use App\System;
use App\View\BaseHtmlView;
use Joomla\Controller\AbstractController;
use Joomla\DI\ContainerAwareInterface;
use Joomla\DI\Container;
use Joomla\Registry\Registry;

/**
 * Default Controller class for the application
 *
 * @since  1.0
 */
class BaseController extends AbstractController implements ContainerAwareInterface
{
	/**
	 * DI Container
	 *
	 * @var    Container
	 * @since  1.0
	 */
	private $container;

	/**
	 * The default view for the app
	 *
	 * @var    string
	 * @since  1.0
	 */
	protected $defaultView = 'dashboard';

	/**
	 * Model variable.
	 *
	 * @var  BaseModel
	 */
	private $model = null;

	/**
	 * View variable.
	 *
	 * @var  BaseHtmlView
	 */
	private $view = null;

	/**
	 * Execute the controller.
	 *
	 * This is a generic method to execute and render a view and is not suitable for tasks.
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 * @throws  \RuntimeException
	 */
	public function execute()
	{
		// Get the input
		$input = $this->getInput();

		// Task to preform (save, edit, delete)
		$task = $input->getString('task', '');

		if (empty($task))
		{
			$viewClass = $this->getView();
			$viewClass->setLayout('index');
		}
		else
		{
			$viewClass = $this->$task();
		}

		if (is_null($viewClass))
		{
			return false;
		}

		try
		{
			// Render our view.
			echo $viewClass->render();

			return true;
		}
		catch (\Exception $e)
		{
			throw new \RuntimeException(sprintf('Doslo je do greške! Info: ' . $e->getMessage()));
		}
	}

	/**
	 * Get the DI container.
	 *
	 * @return  Container
	 *
	 * @since   1.0
	 */
	public function getContainer()
	{
		return $this->container;
	}

	/**
	 * Set the DI container.
	 *
	 * @param   Container  $container  The DI container.
	 *
	 * @return  $this  Method allows chaining
	 *
	 * @since   1.0
	 */
	public function setContainer(Container $container)
	{
		$this->container = $container;

		return $this;
	}

	/**
	 * Function for returning MVC model.
	 *
	 * @param   string  $model  Model to search for.
	 *
	 * @return  BaseModel|boolean  MVC model or false if not found.
	 */
	public function getModel($model = '')
	{
		if (empty($model))
		{
			if (!is_null($this->model))
			{
				return $this->model;
			}

			$cClass = get_class($this);
			$cClass = basename($cClass);
			$model  = ucfirst(strstr($cClass, 'Controller', true));
		}

		$base   = '\\App';
		$mClass = $base . '\\Model\\' . $model . 'Model';

		// If a model doesn't exist for our view, revert to the default model
		if (!class_exists($mClass))
		{
			return false;
		}
		else
		{
			$this->model = new $mClass($this->getInput(), $this->getContainer()->get('db'), new Registry);
		}

		return $this->model;
	}

	/**
	 * Function for returning MVC view.
	 *
	 * @param   string  $view  View to search for.
	 *
	 * @return  BaseHtmlView|boolean  MVC view or false if not found.
	 */
	public function getView($view = '')
	{
		if (empty($view))
		{
			if (!is_null($this->view))
			{
				return $this->view;
			}

			$cClass = get_class($this);
			$cClass = basename($cClass);
			$view   = ucfirst(strstr($cClass, 'Controller', true));
		}

		// Register the templates paths for the view
		$paths = array();
		$path  = JPATH_TEMPLATES . '/' . strtolower($view) . '/';

		if (is_dir($path))
		{
			$paths[] = $path;
		}

		$input   = $this->getInput();
		$vFormat = $input->getWord('format', 'html');
		$base    = '\\App';
		$vClass  = $base . '\\View\\' . ucfirst($view) . ucfirst($vFormat) . 'View';
		$model   = $this->getModel();

		// If a model doesn't exist for our view, revert to the default model
		if (!class_exists($vClass))
		{
			return false;
		}
		else
		{
			$this->view = new $vClass($this->getApplication(), $model, $paths);
		}

		return $this->view;
	}

	/**
	 * Basic save function.
	 *
	 * @return  BaseHtmlView
	 */
	public function save()
	{
		$input = $this->getInput();
		$id    = $input->getInt('id', 0);
		$data  = $input->get('data', array(), 'array');
		$model = $this->getModel();
		$view  = $this->getView();

		if ($id && is_numeric($id))
		{
			$data['id'] = $id;
		}

		if (!$model->save($data))
		{
			$errors = $model->getErrors();

			foreach ($errors as $error)
			{
				System::enqMsg($error, 'error');
			}
		}
		else
		{
			System::enqMsg('Podaci su uspešno sačuvani.');
		}

		$layout = $input->getString('layout', 'index');
		$input->set('layout', $layout);
		$view->setLayout($layout);

		return $view;
	}

	/**
	 * Basic function for view items list/item.
	 *
	 * @return  BaseHtmlView
	 */
	public function view()
	{
		$input = $this->getInput();
		$id    = $input->getInt('id', 0);
		$model = $this->getModel();
		$view  = $this->getView();
		$iView = $input->getString('view', '');

		if (!$model->getItem($id))
		{
			System::enqMsg('Traženi podaci ne postoje!', 'error');

			if (!empty($iView))
			{
				$this->getApplication()->redirect(BASE_URL . $iView . '/index/');
			}
			else
			{
				$this->getApplication()->redirect(BASE_URL);
			}

			return null;
		}

		$layout = $input->getString('layout', 'index');
		$input->set('layout', $layout);
		$view->setLayout($layout);

		return $view;
	}

	/**
	 * Basic function for editing an item.
	 *
	 * @return  BaseHtmlView
	 */
	public function edit()
	{
		$input = $this->getInput();
		$id    = $input->getInt('id', 0);
		$model = $this->getModel();
		$view  = $this->getView();
		$iView = $input->getString('view');

		if (!$model->getItem($id))
		{
			System::enqMsg('Traženi podaci ne postoje!', 'error');

			if (!empty($iView))
			{
				$this->getApplication()->redirect(BASE_URL . $iView . '/index/');
			}
			else
			{
				$this->getApplication()->redirect(BASE_URL);
			}

			return null;
		}

		$layout = $input->getString('layout', 'edit');
		$input->set('layout', $layout);
		$view->setLayout($layout);

		return $view;
	}

	/**
	 * Basic function for deleting item from the list.
	 *
	 * @return  BaseHtmlView
	 */
	public function delete()
	{
		$input = $this->getInput();
		$ids   = $input->get('ids', array(), 'array');
		$model = $this->getModel();
		$view  = $this->getView();
		$iView = $input->getString('view');

		if (!$model->delete($ids))
		{
			System::enqMsg('Brisanje je neuspešno!', 'error');

			if (!empty($iView))
			{
				$this->getApplication()->redirect(BASE_URL . $iView . '/index/');
			}
			else
			{
				$this->getApplication()->redirect(BASE_URL);
			}

			return null;
		}

		$layout = $input->getString('layout', 'index');
		$input->set('layout', $layout);
		$view->setLayout($layout);

		return $view;
	}

	/**
	 * Basic function for adding record.
	 *
	 * @return  BaseHtmlView
	 */
	public function add()
	{
		return $this->save();
	}

	/**
	 * Ajax function for getting items list.
	 *
	 * @return  void
	 */
	public function ajaxGetItems()
	{
		$input   = $this->getInput();
		$type    = $input->getString('type', 'patient');
		$start   = $input->getInt('start', 0);
		$limit   = $input->getInt('length', 10);
		$draw    = $input->getInt('draw', 0);
		$order   = $input->get('order', array(), 'array');
		$columns = $input->get('columns', array(), 'array');
		$search  = $input->get('search', array(), 'array');
		$model   = $this->getModel();
		$items   = $model->ajaxGetItems($type, $start, $limit, $draw, $columns, $order, $search);

		echo json_encode($items);
	}
}
