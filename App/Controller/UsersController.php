<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Controller;

/**
 * Users Controller class for the Application
 *
 * @since  1.0
 */
class UsersController extends BaseController
{
	/**
	 * The default view for the app
	 *
	 * @var    string
	 * @since  1.0
	 */
	protected $defaultView = 'users';

	/**
	 * Ajax function for getting items list.
	 *
	 * @return  void
	 */
	public function ajaxGetItems()
	{
		$input   = $this->getInput();
		$type    = $input->getString('type', 'patient');
		$start   = $input->getInt('start', 0);
		$limit   = $input->getInt('length', 10);
		$draw    = $input->getInt('draw', 0);
		$order   = $input->get('order', array(), 'array');
		$columns = $input->get('columns', array(), 'array');
		$search  = $input->get('search', array(), 'array');
		$model   = $this->getModel();
		$items   = $model->ajaxGetItems($type, $start, $limit, $draw, $columns, $order, $search);

		echo json_encode($items);
	}
}
