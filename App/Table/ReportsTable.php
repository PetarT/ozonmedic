<?php
/**
 * Reports database table class.
 *
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Table;
use Joomla\Database\DatabaseDriver;

/**
 * Reports Database Table class
 *
 * @since  1.0
 */
class ReportsTable extends BaseDatabaseTable
{
	/**
	 * Constructor
	 *
	 * @param   DatabaseDriver  $db  A database connector object
	 *
	 * @since   1.0
	 */
	public function __construct(DatabaseDriver $db)
	{
		parent::__construct($db, '#__examinations', 'id');
	}
}