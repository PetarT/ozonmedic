<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Model;
use Joomla\Database\DatabaseDriver;
use Joomla\Input\Input;
use Joomla\Registry\Registry;

/**
 * Class UsersModel
 *
 * @package  App\Model
 * @since    1.0
 */
class UsersModel extends BaseModel
{
	/**
	 * UsersModel constructor.
	 *
	 * @param   Input           $input  Input object.
	 * @param   DatabaseDriver  $db     Database driver object.
	 * @param   Registry        $state  Registry object.
	 */
	public function __construct(Input $input, DatabaseDriver $db, Registry $state)
	{
		$type = $input->getString('type', 'patient');

		switch ($type)
		{
			case 'patient' :
				$this->columns = array(
					array('db' => 'name', 'dt' => 1),
					array('db' => 'phone', 'dt' => 2),
					array('db' => 'mobile', 'dt' => 3),
					array('db' => 'address', 'dt' => 4),
					array('db' => 'city', 'dt' => 5),
					array('db' => 'jmbg', 'dt' => 6),
					array('db' => 'lk', 'dt' => 7),
					array('db' => 'email', 'dt' => 8),
					array('db' => 'barcode', 'dt' => 9),
					array(
						'db'        => 'created_time',
						'dt'        => 10,
						'formatter' => function($d, $row) {
							return date('jS M y', strtotime($d));
						}
					),
					array('db' => 'created_by', 'dt' => 11)
				);

				break;
			case 'therapist' :
			case 'doctor'    :
				$this->columns = array(
					array('db' => 'name', 'dt' => 1),
					array('db' => 'phone', 'dt' => 2),
					array('db' => 'mobile', 'dt' => 3),
					array('db' => 'address', 'dt' => 4),
					array('db' => 'city', 'dt' => 5),
					array('db' => 'email', 'dt' => 6),
					array(
						'db'        => 'created_time',
						'dt'        => 7,
						'formatter' => function($d, $row) {
							return date('jS M y', strtotime($d));
						}
					),
					array('db' => 'created_by', 'dt' => 8)
				);

				break;
			default:
				$columns       = parent::getColumns();
				$this->columns = array();
				$i = 1;

				foreach ($columns as $column)
				{
					$this->columns[] = array('db' => $column, 'dt' => $i++);
				}

				break;
		}

		return parent::__construct($input, $db, $state);
	}

	/**
	 * Function for getting table columns for list display.
	 *
	 * @return  array  Array of column names.
	 */
	public function getColumns()
	{
		$type = $this->input->get('type', 'patient');

		switch ($type)
		{
			case 'patient':
				$columns = array(
					'Ime i prezime',
					'Telefon',
					'Mobilni telefon',
					'Adresa',
					'Grad',
					'JMBG',
					'Lična karta',
					'Email adresa',
					'Barkod',
					'Dodat',
					'Dodao'
				);

				break;
			case 'therapist':
			case 'doctor':
				$columns = array(
					'Ime i prezime',
					'Telefon',
					'Mobilni telefon',
					'Adresa',
					'Grad',
					'Email adresa',
					'Dodat',
					'Dodao'
				);

				break;
			default:
				$columns = parent::getColumns();

				break;
		}

		return $columns;
	}
}
