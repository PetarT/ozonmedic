<?php
/**
 * Base model for the application.
 *
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Model;

use Joomla\Form\Form;
use Joomla\Input\Input;
use Joomla\Model\AbstractDatabaseModel;
use Joomla\Database\DatabaseDriver;
use Joomla\Database\DatabaseQuery;
use Joomla\Registry\Registry;
use App\Table\BaseDatabaseTable;

/**
 * Base model for the tracker application.
 *
 * @since  1.0
 */
abstract class BaseModel extends AbstractDatabaseModel
{
	/**
	 * Input object
	 *
	 * @var    Input
	 * @since  1.0
	 */
	protected $input;

	/**
	 * Form object.
	 *
	 * @var Form
	 */
	protected $form;

	/**
	 * Table object for item loading.
	 *
	 * @var  BaseDatabaseTable
	 */
	protected $table = null;

	/**
	 * Query offset value.
	 *
	 * @var  int
	 */
	protected $offset = 0;

	/**
	 * Query limit value.
	 *
	 * @var  int
	 */
	protected $limit = 20;

	/**
	 * Last loaded item.
	 *
	 * @var  object
	 */
	private static $item = null;

	/**
	 * Items array.
	 *
	 * @var  array
	 */
	private static $items = array();

	/**
	 * DataTables columns array.
	 *
	 * @var  array
	 */
	public $columns = array();

	/**
	 * Offset getter.
	 *
	 * @return  int  Offset value.
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * Offset setter.
	 *
	 * @param   int  $offset  Offset value.
	 *
	 * @return  void
	 */
	public function setOffset($offset)
	{
		$this->offset = $offset;
	}

	/**
	 * Limit getter.
	 *
	 * @return  int  Limit value.
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 * Limit setter.
	 *
	 * @param   int  $limit  Limit value.
	 *
	 * @return  void
	 */
	public function setLimit($limit)
	{
		$this->offset = $limit;
	}

	/**
	 * Instantiate the model.
	 *
	 * @param   Input           $input  Input object.
	 * @param   DatabaseDriver  $db     The database adapter.
	 * @param   Registry        $state  The model state.
	 *
	 * @since   1.0
	 */
	public function __construct(Input $input, DatabaseDriver $db, Registry $state = null)
	{
		parent::__construct($db, $state);

		$this->input = $input;
		$this->form  = $this->getForm();
		$this->table = $this->getTable();
	}

	/**
	 * Get model Form object.
	 *
	 * @param   string  $name  Form name.
	 *
	 * @return  Form  Form object for processing fields.
	 */
	public function getForm($name = '')
	{
		if (empty($name) && !isset($this->form))
		{
			$class = get_class($this);
			$name  = ucfirst(strstr($class, 'Model', true));

			$this->form = new Form($name);
			$filePath   = JPATH_ROOT . '/Model/Forms/' . $name . '.xml';

			if (file_exists($filePath))
			{
				$this->form->loadFile($filePath);
			}
		}
		elseif (!empty($name))
		{
			$form     = new Form($name);
			$filePath = JPATH_ROOT . '/Model/Forms/' . $name . '.xml';

			if (file_exists($filePath))
			{
				$form->loadFile($filePath);
			}

			return $form;
		}

		return $this->form;
	}

	/**
	 * Function for getting items query.
	 *
	 * @return  DatabaseQuery  Query string for getting items list.
	 */
	public function getListQuery()
	{
		$tableName = $this->table->getTableName();
		$query     = $this->db->getQuery(true);
		$query->select('*')
			->from($this->db->qn($tableName));

		$filters = $this->input->get('filters', array(), 'array');

		foreach ($filters as $key => $value)
		{
			$filter = $this->db->q('%' . $value . '%');
			$query->where($this->db->qn($key) . ' LIKE ' . $filter);
		}

		return $query;
	}

	/**
	 * Function for getting table items.
	 *
	 * @return  array  Array of items objects.
	 */
	public function getItems()
	{
		if (empty(self::$items))
		{
			$db    = $this->getDb();
			$query = $this->getListQuery();

			$db->setQuery($query, $this->offset, $this->limit);

			self::$items = $db->loadObjectList();
		}

		return array_values(self::$items);
	}

	/**
	 * Set page to load.
	 *
	 * @param   int  $page   Page number.
	 * @param   int  $limit  Limit per page.
	 *
	 * @return  void
	 */
	public function setPage($page = 0, $limit = 20)
	{
		$this->limit = $limit;

		if ($page > 0)
		{
			$this->offset = ($page - 1) * $this->limit;
		}
	}

	/**
	 * Function for getting item from the table.
	 *
	 * @param   int  $id  Item id to load.
	 *
	 * @return  object|false  Item object or null if object loading fails.
	 */
	public function getItem($id = 0)
	{
		if (!isset(self::$item))
		{
			if (!isset($this->table))
			{
				$class       = get_class($this);
				$name        = ucfirst(strstr($class, 'Model', true)) . 'Table';
				$this->table = new $name($this->db);
			}

			$item = $this->table->load($id);

			if (!empty($item))
			{
				self::$item = $item;
			}
			else
			{
				return false;
			}
		}

		return self::$item;
	}

	/**
	 * Function for getting model table.
	 *
	 * @param   string  $name  Table name to fetch.
	 *
	 * @return  BaseDatabaseTable
	 */
	public function getTable($name = '')
	{
		if (!empty($name) || !isset($this->table))
		{
			$class = get_class($this);
			$class = basename($class);

			if (empty($name))
			{
				$name = '\\App\\Table\\' . ucfirst(strstr($class, 'Model', true)) . 'Table';
			}
			else
			{
				$name = '\\App\\Table\\' . ucfirst($name) . 'Table';
			}

			$this->table = new $name($this->db);
		}

		return $this->table;
	}

	/**
	 * Save model data.
	 *
	 * @param   array  $data  Data from the request to store.
	 *
	 * @return  boolean  True on success, false otherwise.
	 */
	public function save($data)
	{
		if (!empty($data))
		{
			if (!isset($this->table))
			{
				$this->table = $this->getTable();
			}

			if ($this->table->save($data))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Delete model data.
	 *
	 * @param   array  $ids  Ids to delete.
	 *
	 * @return  boolean  True on success, false otherwise.
	 */
	public function delete($ids)
	{
		if (!empty($ids))
		{
			if (!isset($this->table))
			{
				$this->table = $this->getTable();
			}

			if ($this->table->delete($ids))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Function for getting table columns for list display.
	 *
	 * @return  array  Array of column names.
	 */
	public function getColumns()
	{
		if (!isset($this->table))
		{
			$this->table = $this->getTable();
		}

		$columns = array_keys($this->table->getFields());

		return $columns;
	}

	/**
	 * Ajax method for getting items from database using dataTables.js logic.
	 *
	 * @param $start
	 * @param $limit
	 * @param $draw
	 * @param $columns
	 * @param $order
	 * @param $search
	 *
	 * @return array
	 */
	public function ajaxGetItems($start, $limit, $draw, $columns, $order, $search)
	{
		return array();
	}
}
