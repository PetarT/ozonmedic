<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\Model;

/**
 * Class CalendarModel
 *
 * @package  App\Model
 * @since    1.0
 */
class CalendarModel extends BaseModel
{
}
