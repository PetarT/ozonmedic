<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\View;

/**
 * Users HTML view class for the application
 *
 * @since  1.0
 */
class UsersHtmlView extends BaseHtmlView
{
	/**
	 * Method to render the view.
	 *
	 * @return  string  The rendered view.
	 *
	 * @since   1.0
	 * @throws  \RuntimeException
	 */
	public function render()
	{
		$type = $this->app->input->getString('type', 'patient');

		switch ($type)
		{
			case 'doctor':
				$this->renderer->set('uType', 'doktora');

				break;
			case 'therapist':
				$this->renderer->set('uType', 'terapeuta');

				break;
			case 'patient' :
			default        :
				$this->renderer->set('uType', 'pacijenata');

				break;
		}

		$this->renderer->set('type', $type);

		return parent::render();
	}
}
