<?php
/**
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App\View;

/**
 * Dashboard HTML view class for the application
 *
 * @since  1.0
 */
class DashboardHtmlView extends BaseHtmlView
{
	/**
	 * Method to render the view.
	 *
	 * @return  string  The rendered view.
	 *
	 * @since   1.0
	 * @throws  \RuntimeException
	 */
	public function render()
	{
		$this->renderer->set('dbConnectionSet', $this->app->input->get('dbConnectionSet', false));
		$this->renderer->set('dbTablesSet', $this->app->input->get('dbTablesSet', false));
		$this->renderer->set('config', $this->app->getContainer()->get('config'));

		return $this->renderer->render($this->layout);
	}
}
