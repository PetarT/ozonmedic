<?php
/**
 * Database strategy for authentication.
 *
 * @copyright  Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

namespace Security\Strategies;

use Joomla\Authentication\Authentication;
use Joomla\Authentication\Strategies\DatabaseStrategy;

/**
 * Joomla Framework Database Strategy Authentication class
 *
 * @since  1.1.0
 */
class DbStrategy extends DatabaseStrategy
{
	/**
	 * Attempt to authenticate the username and password pair.
	 *
	 * @return  string|boolean  A string containing a username if authentication is successful, false otherwise.
	 *
	 * @since   1.1.0
	 */
	public function authenticate()
	{
		$username = $this->input->get('username', false, 'username');
		$password = $this->input->get('password', false, 'raw');

		if (!$username || !$password)
		{
			$this->status = Authentication::NO_CREDENTIALS;

			return false;
		}

		return $this->doAuthenticate($username, $password);
	}

	/**
	 * Retrieve the hashed password for the specified user.
	 *
	 * @param   string  $username  Username to lookup.
	 *
	 * @return  string|boolean  Hashed password on success or boolean false on failure.
	 *
	 * @since   1.1.0
	 */
	public function getHashedPassword($username)
	{
		$password = $this->db->setQuery(
			$this->db->getQuery(true)
				->select($this->dbOptions['password_column'])
				->from($this->dbOptions['database_table'])
				->where($this->dbOptions['username_column'] . ' = ' . $this->db->quote($username))
		)->loadResult();

		if (!$password)
		{
			return false;
		}

		return $password;
	}
}
