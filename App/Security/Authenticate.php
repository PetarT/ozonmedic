<?php
/**
 * Authentication class.
 *
 * @copyright  Copyright (C) 2012 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace App;
use Joomla\Authentication\Authentication;
use Joomla\Authentication\Strategies\DatabaseStrategy;
use Joomla\Database\DatabaseDriver;
use Joomla\Input\Input;

/**
 * Class Authenticate
 *
 * @package  App
 * @since    1.0
 */
class Authenticate extends Authentication
{
	/**
	 * Authenticate constructor.
	 *
	 * @param   Input           $input    Application input.
	 * @param   DatabaseDriver  $db       Database driver.
	 * @param   array           $options  Optional array for database connection.
	 */
	public function __construct($input, $db, $options)
	{
		$this->addStrategy('db', new DatabaseStrategy($input, $db, $options));
	}
}
